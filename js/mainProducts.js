$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
  
  // clicking button with class "category-button"
  $(".category-button").click(function(){
    // get the data-filter value of the button
    var filterValue = $(this).attr('data-filter');
    
    // show all items
    if(filterValue == "all")
    {
      $(".all").show("slow");
    }
    else
    {   
      // hide all items
      $(".all").not('.'+filterValue).hide("slow");
      // and then, show only items with selected data-filter value
      $(".all").filter('.'+filterValue).show("slow");
    }
  });

});